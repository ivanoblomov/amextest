//
//  FCViewController.m
//  FCAmexTest
//
//  Created by Roderick Monje on 2/19/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import "FCMapController.h"
#import "FCPhotosController.h"
#define kIdentifierPhotosController @"PhotosController"

@interface FCMapController ()

@end

@implementation FCMapController

- (void)addGestureRecognizer {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(foundTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    tapRecognizer.delegate = self;
    [self.mapView addGestureRecognizer:tapRecognizer];
    self.mapView.accessibilityLabel = @"map";
}


-(IBAction)foundTap:(UITapGestureRecognizer *)recognizer {
    CGPoint tapPoint = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapCoord = [self.mapView convertPoint:tapPoint toCoordinateFromView:self.view];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [_locations autorelease];
    [self loadLocations:tapCoord];
}

-(void)loadLocations:(CLLocationCoordinate2D)coordinate {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (FCLocation *location in [FCLocation nearbyPhotos:coordinate radius:[self.radiusSlider value]]) {
            [self.mapView addAnnotation:location];
        }
        [self zoomToPins:self.mapView.annotations];
    });
}


- (NSArray*)locations {
    return self.mapView.annotations;
}

-(FCPhotosController *)photosController {
    FCPhotosController *photosC = [self.storyboard instantiateViewControllerWithIdentifier:kIdentifierPhotosController];
    photosC.delegate = self;
    return photosC;
}

-(void)selectLocation:(FCLocation *)location {
    [self zoomToPins:[NSArray arrayWithObject:location]];
}

-(void)showPhotos {
    [self.navigationController pushViewController:[self photosController] animated:YES];
}

-(void)zoomToPins:(NSArray *)pins {
	MKMapRect zoomRect = MKMapRectNull;

	for (id <MKAnnotation> annotation in pins) {
		if ([annotation isKindOfClass:[MKUserLocation class]])
			continue;
		MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
		MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
		zoomRect = MKMapRectUnion(zoomRect, pointRect);
	}

	[self.mapView setVisibleMapRect:zoomRect animated:YES];
}

-(void)zoomToUserLocation {
    MKCoordinateRegion mapRegion;
    mapRegion.center = self.mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    [self.mapView setRegion:mapRegion animated: YES];
}

#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)theMapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    [self showPhotos];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    if (! self.mapView.annotations || self.mapView.annotations.count <= 1) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self zoomToUserLocation];
        });
    }
}

#pragma mark NSObject

- (void)dealloc {
    [_mapView autorelease];
    [_radiusSlider autorelease];
    for (id location in _locations)
        [location autorelease];
    [_locations autorelease];
    [super dealloc];
}

#pragma mark UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	if ([touch.view isKindOfClass:[MKAnnotationView class]]) {
        [self showPhotos];
		return NO;
    } else
        return YES;
}

#pragma mark UIViewController

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	[self becomeFirstResponder];
	[self.mapView setDelegate:self];
	[self.mapView setShowsUserLocation:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

@end