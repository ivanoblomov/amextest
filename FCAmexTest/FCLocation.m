//
//  FCLocation.m
//  FCAmexTest
//
//  Created by Roderick Monje on 2/19/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import "FCLocation.h"
#define kInstagramAPIKey @"69274799c146449b8b9da50510ac4f1d"
#define kInstagramURLMediaSearch @"https://api.instagram.com/v1/media/search?lat=%@&lng=%@&distance=%f&client_id=%@"

@implementation FCLocation

+ (NSArray*)nearbyPhotos:(CLLocationCoordinate2D)coordinate radius:(double)radius {
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    NSArray *photoDictionaries = [self photoDictionariesNearCoordinate:coordinate radius:radius];
    for (id photoDictionary in photoDictionaries)
        [photos addObject:[self locationWithDictionary:photoDictionary coordinate:coordinate]];
    return [photos autorelease];
}

+ (NSArray*)photoDictionariesNearCoordinate:(CLLocationCoordinate2D)coordinate radius:(double)meters {
    NSString *latString = [[NSString alloc] initWithFormat:@"%f", coordinate.latitude];
    NSString *longString = [[NSString alloc] initWithFormat:@"%f", coordinate.longitude];
    NSString *url = [NSString stringWithFormat:kInstagramURLMediaSearch, [latString autorelease], [longString autorelease], meters, kInstagramAPIKey];
	return [[NSDictionary objectWithContentsOfJSONURLString:url] objectForKey:@"data"];
}

+ (FCLocation *)locationWithDictionary:(NSDictionary *)dictionary coordinate:(CLLocationCoordinate2D)coordinate {
	if (dictionary) {
		FCLocation *location = [[FCLocation alloc] init];
        NSDictionary *locationDictionary = [dictionary objectForKey:@"location"];
        NSString *latitude = [locationDictionary objectForKey:@"latitude"];
        NSString *longitude = [locationDictionary objectForKey:@"longitude"];
		location.coordinate = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
        [location setDistanceToCoordinate:coordinate];
        NSDictionary *userDictionary = [dictionary objectForKey:@"user"];
        location.fullName = [userDictionary objectForKey:@"full_name"];
        location.username = [userDictionary objectForKey:@"username"];
        NSDictionary *imagesDictionary = [dictionary objectForKey:@"images"];
        NSDictionary *thumbnailDictionary = [imagesDictionary objectForKey:@"thumbnail"];
        location.url = [thumbnailDictionary objectForKey:@"url"];
		return [location autorelease];
	} else
		return nil;
}

- (void)setDistanceToCoordinate:(CLLocationCoordinate2D)coordinate {
    CLLocation *photoLocation = [[CLLocation alloc] initWithLatitude:self.coordinate.latitude longitude:self.coordinate.longitude];
    CLLocation *tapLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    self.distance = [photoLocation distanceFromLocation:tapLocation];
    [photoLocation autorelease];
    [tapLocation autorelease];
}


- (NSString *)subtitle {
    return [NSString stringWithFormat:@"%.0f m", self.distance];
}

- (NSString *)title {
    return self.username;
}

#pragma mark NSObject

- (void)dealloc {
    [_fullName autorelease];
    [_subtitle autorelease];
    [_title autorelease];
    [_url autorelease];
    [_username autorelease];
    [super dealloc];
}

@end