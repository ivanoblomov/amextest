//
//  FCLocation.h
//  FCAmexTest
//
//  Created by Roderick Monje on 2/19/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "NSObject+JSONObject.h"

@interface FCLocation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic) CLLocationDistance distance;
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *username;

+(NSArray *) nearbyPhotos:(CLLocationCoordinate2D)coordinate radius:(double)radius;

@end