//
//  FCPhotosController.m
//  FCAmexTest
//
//  Created by Roderick Monje on 2/20/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import "FCPhotosController.h"
#import "FCMapController.h"

@implementation FCPhotosController

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.delegate selectLocation:[self.delegate.locations objectAtIndex:indexPath.row]];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark UIViewCollectionDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.delegate.locations.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FCLocation *location = [self.delegate.locations objectAtIndex:indexPath.row];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoCell" forIndexPath:indexPath];
    if ([location respondsToSelector:@selector(username)]) {
        [(UILabel *) [cell viewWithTag:1] setText:location.username];
        [(UILabel *) [cell viewWithTag:2] setText:location.subtitle];
        [(UIImageView *) [cell viewWithTag:3] setImageWithURL:[NSURL URLWithString:location.url] placeholderImage:[UIImage imageNamed:@"pin"]];
    }
    return cell;
}

#pragma mark UIViewController

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

@end