//
//  FCPhotosController.h
//  FCAmexTest
//
//  Created by Roderick Monje on 2/20/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <UIKit/UIKit.h>
#import "FCMapController.h"

@interface FCPhotosController : UICollectionViewController

@property (retain, nonatomic) FCMapController *delegate;

@end
