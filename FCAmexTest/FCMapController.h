//
//  FCViewController.h
//  FCAmexTest
//
//  Created by Roderick Monje on 2/19/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>
#import "FCLocation.h"

@interface FCMapController : UIViewController <MKMapViewDelegate, UIGestureRecognizerDelegate>

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) IBOutlet UISlider *radiusSlider;
@property (retain, nonatomic) NSMutableArray *locations;

-(void)selectLocation:(FCLocation *)location;

@end