//
//  main.m
//  FCAmexTest
//
//  Created by Roderick Monje on 2/19/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FCAppDelegate class]));
    }
}
