//  NSObject+JSONObject.h
//  NovelsOnLocation
//
//  Copyright © 2013 Roderick Monje

#import <objc/runtime.h>

#define kErrorMessageJSONRequest @"JSON request %@ failed: %@!"

@interface NSObject (JSONObject)

+ (id)objectWithContentsOfJSONURLString:(NSString *)urlString;
- (NSData*)toJSON:(NSString *)keyName;

@end