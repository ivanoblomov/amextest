//
//  FCSimpleTest.m
//  FCAmexTest
//
//  Created by Roderick Monje on 2/21/14.
//  Copyright (c) 2014 Fovea Central. All rights reserved.
//

#import <Subliminal/Subliminal.h>

@interface FCSimpleTest : SLTest

@end

@implementation FCSimpleTest

- (void)setUpTest {
	// Navigate to the part of the app being exercised by the test cases,
	// initialize SLElements common to the test cases, etc.
}

- (void)tearDownTest {
	// Navigate back to "home", if applicable.
}

- (void)testCase {
    SLElement *map = [SLElement elementWithAccessibilityLabel:@"map"];
    [map tap];
    SLAssertTrueWithTimeout(NO, 3.0, @"Can't find map");
}

@end
